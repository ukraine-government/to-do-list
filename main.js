// DOM
const mainList = document.querySelector('UL');
const addBtn = document.getElementById('addBtn');
const toggleBtn = document.getElementById('toggle');
const input = document.getElementById('input');
// Render

let toDo = [];

if (localStorage.getItem('toDo')) {
    let render = JSON.parse(localStorage.getItem('toDo'));
    render.forEach(el => {
        if (el.id) {
            const newLi = document.createElement('li');
            newLi.innerHTML = `<p id="id">${el.id}</p>${el.task}<span class=check>✔</span><span class="close">×</span>`;
            mainList.append(newLi);
            if (el.status) {
                newLi.classList.add('checked');
            }
        }
    });
}

// Events

mainList.addEventListener('click', e => {
    const val = Number(e.target.parentNode.firstChild.textContent);
    if (toggleBtn.classList.contains('active')) {
        e.target.classList.toggle('hidden');
    }
    if (e.target.classList.contains('check')) {
        if (e.target.parentElement.classList.contains('checked')) {
            let toDo = JSON.parse(localStorage.getItem('toDo'));
            let find = toDo.find(el => el.id === val);
            find.status = false;
            localStorage.setItem('toDo', JSON.stringify(toDo));

        } else {
            let toDo = JSON.parse(localStorage.getItem('toDo'));
            let find = toDo.find(el => el.id === val);
            find.status = true;
            localStorage.setItem('toDo', JSON.stringify(toDo));
        }
        e.target.parentElement.classList.toggle('checked');
    }
    if (e.target.classList.contains('check') && toggleBtn.classList.contains('active')) {
        e.target.parentElement.classList.toggle('hidden');
    }
    if (e.target.classList.contains('close')) {
        e.target.parentElement.remove();
        let toDo = JSON.parse(localStorage.getItem('toDo'));
        let find = toDo.find(el => el.id === val);
        find.id = '';
        localStorage.setItem('toDo', JSON.stringify(toDo));
    }
});

// Add Function
function add() {
    if (input.value === '') {
        alert('Write Something!');
    } else {
        const newLi = document.createElement('li');
        let val;
        if (document.body.contains(mainList.firstElementChild)) {
            val = Number(mainList.lastElementChild.firstChild.textContent);
        } else {
            val = 0;
        }
        val++;
        newLi.innerHTML = `<p id="id">${val}</p>${input.value}<span class=check>✔</span><span class="close">×</span>`;
        mainList.append(newLi);
        let el = {
            id: val,
            task: input.value,
            status: false
        }
        toDo.push(el);
        localStorage.setItem('toDo', JSON.stringify(toDo));

        input.value = '';
    }
}

addBtn.addEventListener('click', () => add());

input.addEventListener('keypress', e => {
    if (e.which === 13) {
        add();
    }
});

// Show/Hide active tasks
toggleBtn.addEventListener('click', () => {
    toggleBtn.classList.toggle('active');
    document.querySelectorAll('.checked').forEach(el => {
        el.classList.toggle('hidden');
    })
});